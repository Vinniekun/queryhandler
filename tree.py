class Node(object):

    def __init__(self, tipo, texto, operador = ''):
        self.tipo = tipo
        self.texto = texto
        self.operador = operador
        self.filhos = []
        self.pai = None
        self.altura = 0

    def setPai(self, nodepai):
        self.pai = nodepai
        self.altura = nodepai.getAlturaNo() + 1

    #0->esquerda 1->direita
    def addFilho(self, node):
        if len(self.filhos) > 2:
            return False
        else:
            node.setPai(self)
            self.filhos.append(node)

    def getTipo(self):
        return self.tipo

    def getAlturaNo(self):
        return self.altura

    def getTexto(self):
        return self.texto

    def getPai(self):
        return self.pai

    def getFilhos(self):
        return self.filhos

    def getOperador(self):
        return self.operador

    def getAllofType(self, tipo):
        if len(self.filhos) == 0:
            lista = []
        else:
            lista = self.filhos[0].getAllofType(tipo)
            if self.tipo == 'X':
                lista += self.filhos[1].getAllofType(tipo)
        if self.tipo == tipo:
            lista.append(self)
        return lista

    def getAllTextsofType(self,tipo):
        if len(self.filhos) == 0:
            lista = []
        else:
            lista = self.filhos[0].getAllofType(tipo)
            if self.tipo == 'X':
                lista += self.filhos[1].getAllofType(tipo)
        if self.tipo == tipo:
            lista.append(self.texto)
        return lista

    def getAltura(self):
        if len(self.filhos):
            return 1 + max([filho.getAltura() for filho in self.filhos])
        else:
            return 1

    def remove(self):
        if self.pai is not None:
            self.pai.filhos.remove(self)
            for filho in self.filhos:
                self.pai.addFilho(filho)
            self.pai = None
            self.filhos = []

    def enxerto(self, nodedown):
        nodedown.pai.filhos.remove(nodedown)
        nodedown.pai.addFilho(self)
        self.addFilho(nodedown)

    def imprimir(self):
        if(self is not None):
            print(self.tipo,self.texto,self.getAltura(self),len(self.filhos))
            for filhos in self.filhos:
                self.imprimir(filhos)
