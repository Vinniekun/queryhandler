# Extraído de https://gmigdos.wordpress.com/2014/06/29/how-to-bundle-python-gtk3-apps-on-windows-with-py2exe/
# Necessária instalação do pacote py2exe -> $ python -m pip install py2exe
# execução com $ python setup.py py2exe

from distutils.core import setup 
import py2exe 
import sys, os, site, shutil
  
site_dir = site.getsitepackages()[1] 
include_dll_path = os.path.join(site_dir, "gnome") 
  
gtk_dirs_to_include = ['etc', 'lib\\gtk-3.0', 'lib\\girepository-1.0', 'lib\\gio', 'lib\\gdk-pixbuf-2.0', 'share\\glib-2.0', 'share\\fonts', 'share\\icons', 'share\\themes\\Default', 'share\\themes\\HighContrast'] 
  
gtk_dlls = [] 
tmp_dlls = [] 
cdir = os.getcwd() 
for dll in os.listdir(include_dll_path): 
    if dll.lower().endswith('.dll'): 
        gtk_dlls.append(os.path.join(include_dll_path, dll)) 
        tmp_dlls.append(os.path.join(cdir, dll)) 
  
for dll in gtk_dlls: 
    shutil.copy(dll, cdir) 
          
setup(windows=['MainWindow.py'], options={ 
    'py2exe': { 
        'includes' : ['gi',"pygame","ctypes"], 
        'packages': ['gi',"pygame","ctypes"] 
    } 
}) 
  
dest_dir = os.path.join(cdir, 'dist') 
for dll in tmp_dlls: 
    shutil.copy(dll, dest_dir)
  
for d  in gtk_dirs_to_include: 
    shutil.copytree(os.path.join(site_dir, "gnome", d), os.path.join(dest_dir, d))

shutil.copy(os.path.join(cdir, 'BebasNeue.otf'), dest_dir)
