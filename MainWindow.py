# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from PygameArea import *
from Draw import *
from queryhandler import *

class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Query Handler")
        self.set_size_request(800, 600)
        self.startLayout()
        self.desenho = None
        self.queryhandler = QueryHandler()

    # Funcao responsavel por criar e iniciar os componentes da interface
    def startLayout(self):

        # Define um gride para estruturar o layout
        grid = Gtk.Grid()
        self.add(grid)

        # Cria o label titulo
        lbTitulo = Gtk.Label()
        lbTitulo.set_markup("<span font_desc='Cantarell Bold 16'>Query Handler</span>")
        lbTitulo.set_hexpand(True)
        lbTitulo.set_vexpand(False)
        lbTitulo.set_padding(5,10)
        grid.attach(lbTitulo, 0, 0, 10, 1) # left, top, width, height

        # Cria o label consulta
        lbConsulta = Gtk.Label()
        lbConsulta.set_markup("<span font_desc='Cantarell Bold 12'>Consulta:</span>")
        lbConsulta.set_hexpand(True)
        lbConsulta.set_vexpand(False)
        lbConsulta.set_halign(1)
        lbConsulta.set_margin_left(10)
        lbConsulta.set_margin_right(10)
        grid.attach(lbConsulta, 0, 1, 10, 1)

        # Cria caixa de texto da consulta
        entry = Gtk.Entry()
        entry.set_text("P nome (S Cliente.nome = \"Vinicius\" AND Func.salario > 2000 (( (Cliente |X| a = b (Func)) |X| Categoria )  |X| Empresa))")
        entry.set_hexpand(True)
        entry.set_vexpand(False)
        entry.set_margin_left(10)
        entry.set_margin_right(10)
        grid.attach(entry, 0, 2, 9, 1)

        # Cria o botao executar
        btExecutar = Gtk.Button(label="Executar")
        btExecutar.connect("clicked", self.on_btExecutar_clicked)
        btExecutar.set_margin_left(10)
        btExecutar.set_margin_right(10)
        grid.attach(btExecutar, 9, 2, 1, 1)

        # Cria o label algebra
        lbAlgebra = Gtk.Label()
        lbAlgebra.set_markup("<span font_desc='Cantarell Bold 12'>Árvore de álgebra relacional:</span>")
        lbAlgebra.set_hexpand(True)
        lbAlgebra.set_vexpand(False)
        lbAlgebra.set_halign(1)
        lbAlgebra.set_margin_left(10)
        lbAlgebra.set_margin_right(10)
        lbAlgebra.set_margin_top(20)
        grid.attach(lbAlgebra, 0, 3, 10, 1)
        self.entry = entry

        # Cria o panel da arvore de algebra
        daAlgebra = PygameArea(780, 416)
        daAlgebra.set_hexpand(True)
        daAlgebra.set_vexpand(True)
        daAlgebra.set_margin_left(10)
        daAlgebra.set_margin_right(10)
        daAlgebra.set_margin_top(20)
        daAlgebra.set_margin_bottom(20)
        daAlgebra.connect("size-allocate",daAlgebra.setSizeArea)
        grid.attach(daAlgebra, 0, 4, 10, 1)
        self.daAlgebra = daAlgebra

    # Dispara o MessageDialog
    def messageDialog(self, widget, m):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "Erro na consulta!")
        dialog.format_secondary_text(str(m))
        dialog.run()
        dialog.destroy()


    # Funcao disparada quando botao Executar e clicado
    def on_btExecutar_clicked(self, widget):
        query = self.entry.get_text()
        arvore = self.queryhandler.gera_no(query)
        if(arvore.__class__ == str):
            self.messageDialog(self, arvore)
        else:
            self.desenho.render_tree(arvore,query)
            # otimizacao
            arvore = self.queryhandler.otimizar(arvore)
            self.desenho.render_tree_otimizada(arvore)
            #self.daAlgebra.screen_update()

win = MainWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
win.desenho = Draw()
Gtk.main()
