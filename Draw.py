import pygame
from tree import *

class Draw():
    def __init__(self):
        pygame.font.init()
        self.font1 = pygame.font.Font("BebasNeue.otf", 28)
        self.font2 = pygame.font.Font("BebasNeue.otf", 14)
        self.projecao = "P"
        self.selecao = "S"
        self.juncao = "|X|"
        self.x = pygame.display.get_surface().get_width()/4
        self.y = 10
        self.inicio = 0
        self.join_x = 0
        self.join_y = 0

    def render_tree(self,tree,query):
        self.draw_area = pygame.display.get_surface()
        self.draw_area.fill((255,255,255))
        self.inicio = 0
        self.y = 35
        self.x = pygame.display.get_surface().get_width() / 4
        texto = self.font2.render(query, True, (0, 0, 0))
        tipo = self.font2.render("Não otimizada",True, (0,0,0))
        tipo2 = self.font2.render("Otimizada", True, (0, 0, 0))
        self.draw_area.blit(tipo, (self.x-25,20))
        self.draw_area.blit(tipo2, (((pygame.display.get_surface().get_width()*3)/4)-20, 20))
        self.draw_area.blit(texto, (0, 0))
        self.print_arv(tree)

    def render_tree_otimizada(self, tree):
        self.draw_area = pygame.display.get_surface()
        #self.draw_area.fill((255, 255, 255))
        self.inicio = 0
        self.y = 35
        self.x = (pygame.display.get_surface().get_width()*3)/4
        self.print_arv(tree)

    def print_arv(self,tree):
        if tree.__class__ is not str:
            if self.inicio is 0:
                self.inicio = 1
            else:
                self.y += 30
                pygame.draw.line(self.draw_area,(0,0,0),(self.x+4,self.y),(self.x+4,self.y+20),2)
                self.y += 25
            tipo = self.font1.render(tree.getTipo(), True, (0, 0, 0))
            if tree.getTipo() in ['S','X']:
                op = tree.operador.center(len(tree.operador)+2)
            elif tree.getTipo() == 'P':
                op = ", "
            else:
                op = ''

            if tree.getTipo() == 'X':
                texto = self.font2.render(op.join(tree.getTexto()), True, (0, 0, 0))
                self.draw_area.blit(tipo, (self.x, self.y))
                self.draw_area.blit(texto, (self.x + 15, self.y + 15))
                ############
                salva_y = self.y
                self.y += 30
                #####Primeiro Filho
                self.inicio = 0
                salva_x = self.x

                self.x = self.x - 50 * tree.getAltura()/2
                pygame.draw.line(self.draw_area, (0, 0, 0), (salva_x, self.y), (self.x+4, self.y + 30), 2)
                self.y += 30
                self.print_arv(tree.filhos[0])
                self.y -= 30


                self.x = salva_x
                self.y = salva_y + 30
                ###Filho direita
                self.inicio = 0
                self.x = self.x + 100 * tree.getAltura()/2
                pygame.draw.line(self.draw_area, (0, 0, 0), (salva_x+10, self.y), (self.x+4, self.y + 30), 2)
                self.y += 30
                self.print_arv(tree.filhos[1])
                self.x = salva_x
                self.y = salva_y


            else:
                texto = self.font2.render(op.join(tree.getTexto()), True, (0, 0, 0))
                self.draw_area.blit(tipo, (self.x, self.y))
                self.draw_area.blit(texto, (self.x+15, self.y+15))
                for filhos in tree.filhos:
                    self.print_arv(filhos)