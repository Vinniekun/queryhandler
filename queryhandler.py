from pygame import *
from tree import Node

def find_matching_parenthesis(query):
    count = 1
    for i in range(1, len(query)):
        if query[i] == '(':
            count += 1
        elif query[i] == ')':
            count -= 1
        if count == 0:
            pos = i
            break
    return pos

def remove_space(lista):
    while True:
        try:
            lista.remove('')
        except:
            break

class QueryHandler():
    def pre_checa_erro(self, query):
        # checa erros simples de sintaxe
        if query.count('(') != query.count(')'):
            return 'Parenteses nao batem'
        return None

    def pos_choca_erros(self, no):
        # checa erros usando a arvore
        tabelas = no.getAllTextsofType('T')
        lista = no.getAllTextsofType('P')
        for colunas in lista:
            for coluna in colunas:
                if coluna.count('.') > 1:
                    return 'Coluna inexistente: '+coluna
                elif coluna.count('.') == 1:
                    tabela = coluna[:coluna.find('.')]
                    if not tabela in tabelas:
                        print (tabelas, lista)
                        return 'Tabela nao referenciada: '+tabela
        lista = no.getAllTextsofType('S')
        for operacoes in lista:
            for operacao in operacoes:
                count = 3
                for i in ('=','<','>'):
                    if not i in operacao:
                        count -= 1
                    else:
                        operandos = [d.strip() for d in operacao.split(i)]
                        if '' in operandos:
                            return 'Operando faltante em '+operacao
                        else:
                            for coluna in operandos:
                                if coluna.count('.') > 1:
                                    return 'Coluna inexistente: '+coluna
                                elif coluna.count('.') == 1:
                                    tabela = coluna[:coluna.find('.')]
                                    if not tabela in tabelas:
                                        return 'Tabela nao referenciada: '+tabela
                            break
                if count is 0:
                    return 'Operador nao reconhecido '+operacao
        return None

    def otimizar(self, tree):
        if len(tree.getAllofType('X')) == 0:
            return
        projecoes = tree.getAllofType('P')
        selecoes = tree.getAllofType('S')
        tabelas = tree.getAllofType('T')
        dic_proj = {}
        dic_sel = {}
        for projecao in projecoes:
            for coluna in projecao.getTexto():
                if coluna.count('.') > 0:
                    nome_tabela = coluna[:coluna.find('.')]
                    if dic_proj.get(nome_tabela) is None:
                        dic_proj[nome_tabela] = Node('P',[nome_tabela + ".id" + nome_tabela, coluna])
                    else:
                        dic_proj[nome_tabela].texto.append(coluna)
        for nova in dic_proj.keys():
            table = None
            for t in tabelas:
                if t.getTexto() == nova:
                    table = t
                    break
            dic_proj[nova].enxerto(table)
        proj_novas = []
        for selecao in selecoes:
            exp_to_remove = []
            for expressao in selecao.getTexto():
                print(expressao)
                for i in ('>','<','='):
                    if expressao.find(i) > -1:
                        lis_exp = [e.strip() for e in expressao.split(i)]
                        if selecao.operador == 'OR':
                            for coluna in lis_exp:
                                if coluna.count('.') > 0:
                                    nome_tabela = coluna[:coluna.find('.')]
                                    if dic_proj.get(nome_tabela) is None:
                                        dic_proj[nome_tabela] = Node('P',[nome_tabela + ".id" + nome_tabela, coluna])
                                        proj_novas.append(nome_tabela)
                                    elif dic_proj[nome_tabela].texto.count(coluna) == 0:
                                        dic_proj[nome_tabela].texto.append(coluna)
                        elif selecao.operador in ('AND',''):
                            print(lis_exp)
                            nome_tabela = None
                            if lis_exp[0].count('.') > 0:
                                if lis_exp[1].count('.') > 0:
                                    if lis_exp[0][:lis_exp[0].find('.')] == lis_exp[1][:lis_exp[1].find('.')]:
                                        nome_tabela = lis_exp[0][:lis_exp[0].find('.')]
                                elif lis_exp[1].isdigit() or (lis_exp[1][0] == '"' and lis_exp[1][-1] == '"'):
                                    nome_tabela = lis_exp[0][:lis_exp[0].find('.')]
                            elif (lis_exp[1].count('.') > 0) and (lis_exp[0].isdigit() or (lis_exp[1][0] == '"' and lis_exp[1][-1] == '"')):
                                nome_tabela = lis_exp[1][:lis_exp[1].find('.')]
                            if nome_tabela is not None:
                                exp_to_remove.append(expressao)
                                if dic_sel.get(nome_tabela) is None:
                                    dic_sel[nome_tabela] = Node('S',[expressao], 'AND')
                                else:
                                    dic_sel[nome_tabela].texto.append(expressao)
                        break
            for expressao in exp_to_remove:
                selecao.texto.remove(expressao)
            if len(selecao.texto) == 0:
                if tree == selecao:
                    tree = selecao.filhos[0]
                else:
                    selecao.remove()

        for nova in proj_novas:
            table = None
            for t in tabelas:
                if t.getTexto() == nova:
                    table = t
                    break
            dic_proj[nova].enxerto(table)
        for nova in dic_sel.keys():
            table = None
            for t in tabelas:
                if t.getTexto() == nova:
                    table = t
                    break
            dic_sel[nova].enxerto(table)
        return tree

    def gera_no(self, query):
        query = query.strip()

        if query.split(' ')[0] == 'P':
            # Reconhece Projecoes
            abre = query.find('(')
            if (abre == -1):
                return 'Projecao sem origem'
            clause = query[:abre]
            resto = query[abre+1:-1]
            element_list = clause.replace(',','').split(' ')
            remove_space(element_list)
            tipo = element_list[0]
            texto = element_list[1:]
            no = Node(tipo,texto)
            filho = self.gera_no(resto)
            if filho.__class__ is str:
                return filho
            no.addFilho(filho)

        elif query.split(' ')[0] == 'S':
            # Reconhece Selecoes
            abre = query.find('(')
            if (abre == -1):
                return 'Selecao sem origem'
            clause = query[:abre]
            resto = query[abre + 1:-1]
            tipo = 'S'
            element_list = clause[clause.find(' ')+1:]
            remove_space(element_list)
            if len(element_list) == 0:
                return 'Selecao sem argumentos'
            elif element_list.find('AND') > -1:
                texto = [ e.strip() for e in element_list.split('AND')]
                no = Node(tipo,texto,'AND')
            elif element_list.find('OR') > -1:
                texto = [ e.strip() for e in element_list.split('OR')]
                no = Node(tipo, texto, 'OR')
            else:
                texto = [element_list.strip()]
                no = Node(tipo,texto)
            filho = self.gera_no(resto)
            if filho.__class__ is str:
                return filho
            no.addFilho(filho)

        elif query.find('|X|') > -1:
            # Reconhece Juncoes
            texto = ''
            operando = ''
            if query[0] == '(':
                pos = find_matching_parenthesis(query)
                esquerda = query[1:pos]
            else:
                esquerda = query.split(' ')[0]
                pos = len(esquerda)+1
            direita = query[pos+1:].lstrip(' |X')
            if direita[0] == '(':
                direita = direita[1:-1]
            elif direita.count('=') > 0:
                if not direita.find('=') < direita.find('('):
                    return 'Juncao sem tabela a direita'
                texto = direita[:direita.find('(')]
                if texto.count('AND') > 0:
                    texto = [t.strip() for t in texto.split('AND')]
                    operando = 'AND'
                elif texto.count('OR') > 0:
                    texto = [t.strip() for t in texto.split('OR')]
                    operando = 'OR'
                direita = direita[direita.find('(')+1:-1]
            elif direita.find('.') > -1 or direita.find('>') > -1 or direita.find('<') > -1:
                return 'Juncao com filtragem incorreta'
            no = Node('X',texto, operando)
            filho = self.gera_no(esquerda)
            if filho.__class__ is str:
                return filho
            no.addFilho(filho)
            filho = self.gera_no(direita)
            if filho.__class__ is str:
                return filho
            no.addFilho(filho)
        else:
            # Reconhece Tabelas
            if query == '':
                return 'Tabela nao informada'
            if query.count(' ') > 0:
                return 'Erro de sintaxe'
            no = Node('T',query)
        return no

    def set_query(self,query):
        erro = self.pre_checa_erro(query)
        print(erro)
        if erro is not None:
            return erro
        self.query = query
        no = self.gera_no(self.query)
        if no.__class__ is str:
            return no
        erro = self.pos_choca_erros(no)
        if erro is not None:
            return erro
        return no
