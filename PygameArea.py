import pygame, os, gi, sys
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GdkX11', '3.0')
from gi.repository import GObject, Gtk, GdkX11, Gdk

class PygameArea(Gtk.DrawingArea):
    def __init__(self, width, height):
        Gtk.DrawingArea.__init__(self)
        self.set_events(self.get_events()
              | Gdk.EventMask.BUTTON_PRESS_MASK
              | Gdk.EventMask.BUTTON_RELEASE_MASK
              | Gdk.EventMask.POINTER_MOTION_MASK)
        self.set_size_request(width, height)
        self.size = [width, height]
        self.connect("realize",self._realized)

    def _realized(self, widget, data=None):
        if sys.platform == "win32":
            xid = patch(widget)
        else:
            xid = widget.get_window().get_xid()
        os.putenv('SDL_WINDOWID',str(xid))
        pygame.init()
        pygame.display.set_mode(self.get_size_request(), 0, 0)
        self.screen = pygame.display.get_surface()
        GObject.timeout_add(200, self.screen_update)

    def screen_update(self):
        pygame.display.flip()
        return True

    def setSizeArea(self, widget, gdkrect):
        if widget.get_window() is not None:
            self.size = [gdkrect.width, gdkrect.height]
            if sys.platform == "win32":
                xid = patch(widget)
            else:
                xid = widget.get_window().get_xid()
            os.putenv('SDL_WINDOWID',str(xid))
            self.screen = pygame.display.set_mode(self.size, 0, 0)

"""
Patch para conseguir o ID da DrawingArea no Windows, necessária para
especificar a área de desenho para o Pygame. A função usada originalmente
get_xid() ainda não possui equivalente para plataforma Windows.
Origem: http://stackoverflow.com/a/27236258 
"""
import ctypes
def patch(drawingarea):
    Gdk.threads_enter()            
    #get the gdk window and the corresponding c gpointer
    drawingareawnd = drawingarea.get_property("window")
    #make sure to call ensure_native before e.g. on realize
    if not drawingareawnd.has_native():
        print("Your window is gonna freeze as soon as you move or resize it...")
    ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
    ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [ctypes.py_object]
    drawingarea_gpointer = ctypes.pythonapi.PyCapsule_GetPointer(drawingareawnd.__gpointer__, None)            
    #get the win32 handle
    gdkdll = ctypes.CDLL ("libgdk-3-0.dll")
    hnd = gdkdll.gdk_win32_window_get_handle(drawingarea_gpointer)
    #do what you want with it ... I pass it to a gstreamer videosink
    Gdk.threads_leave()
    return hnd